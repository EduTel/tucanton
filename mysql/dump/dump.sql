CREATE DATABASE IF NOT EXISTS TuCanton;
USE TuCanton;

DROP TABLE IF EXISTS VENTAS;
DROP TABLE IF EXISTS CLIENTES;
DROP TABLE IF EXISTS USUARIOS;
DROP TABLE IF EXISTS EQUIPOS;

CREATE TABLE EQUIPOS (
  id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  nombre VARCHAR(30) NOT NULL,
  activo INT
);
INSERT INTO EQUIPOS (nombre, activo)
VALUES ('Equipo_1', 1 ),
       ('Equipo_2', 1 );
SELECT * FROM EQUIPOS;


CREATE TABLE USUARIOS (
  id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  nombre VARCHAR(30) NOT NULL,
  `EQUIPOS_id` INT UNSIGNED NOT NULL,
  INDEX `fk_USUARIOS_EQUIPOS1_idx` (`EQUIPOS_id` ASC) VISIBLE,
  CONSTRAINT `fk_USUARIOS_EQUIPOS1`
    FOREIGN KEY (`EQUIPOS_id`)
    REFERENCES `TuCanton`.`EQUIPOS` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  activo INT
);
INSERT INTO USUARIOS (nombre, EQUIPOS_id, activo)
VALUES ('Usuario 1', 1, 1 ),
       ( 'Usuario 3', 2, 2 );


CREATE TABLE CLIENTES (
  id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  nombre VARCHAR(30) NOT NULL,
  `USUARIOS_id` INT UNSIGNED NOT NULL,
  INDEX `fk_CLIENTES_USUARIOS1_idx` (`USUARIOS_id` ASC) VISIBLE,
  CONSTRAINT `fk_CLIENTES_USUARIOS1`
    FOREIGN KEY (`USUARIOS_id`)
    REFERENCES `TuCanton`.`USUARIOS` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  activo INT
);
INSERT INTO CLIENTES (nombre, USUARIOS_id, activo)
VALUES ('Cliente_1', 1, 1 ),
       ( 'Cliente_2', 2, 1 );
SELECT * FROM CLIENTES;

CREATE TABLE VENTAS (
  id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  fecha DATE NOT NULL,
  monto Float NOT NULL,
  activo INT,
  `USUARIOS_id` INT UNSIGNED NOT NULL,
  `CLIENTES_id` INT UNSIGNED NOT NULL,
  INDEX `fk_VENTAS_USUARIOS_idx` (`USUARIOS_id` ASC) VISIBLE,
  INDEX `fk_VENTAS_CLIENTES1_idx` (`CLIENTES_id` ASC) VISIBLE,
  CONSTRAINT `fk_VENTAS_USUARIOS`
    FOREIGN KEY (`USUARIOS_id`)
    REFERENCES `TuCanton`.`USUARIOS` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_VENTAS_CLIENTES1`
    FOREIGN KEY (`CLIENTES_id`)
    REFERENCES `TuCanton`.`CLIENTES` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);
INSERT INTO VENTAS (fecha, monto, CLIENTES_id, USUARIOS_id, activo)
VALUES ( '2021-05-03 12:30:00', 35000, 1, 1, 1 ),
       ( '2021-05-04 12:30:00', 20000, 1, 2, 1 ),
       ( '2021-05-05 12:30:00', 25000, 2, 1, 1 ),
       ( '2021-05-06 12:30:00', 11000, 2, 2, 1 ),
       ( '2021-06-05 12:30:00', 40000, 1, 1, 1 ),
       ( '2021-07-06 12:30:00', 10000, 2, 2, 1 );
/*
VALUES ( STR_TO_DATE('2021-05-03 12:30:00', '%Y-%m-%d %H:%M:%S'), 35000, 1, 1, 1 ),
       ( STR_TO_DATE('2021-05-04 12:30:00', '%Y-%m-%d %H:%M:%S'), 20000, 1, 2, 1 ),
       ( STR_TO_DATE('2021-05-05 12:30:00', '%Y-%m-%d %H:%M:%S'), 25000, 2, 1, 1 ),
       ( STR_TO_DATE('2021-05-06 12:30:00', '%Y-%m-%d %H:%M:%S'), 11000, 2, 2, 1 ),
       ( STR_TO_DATE('2021-06-05 12:30:00', '%Y-%m-%d %H:%M:%S'), 40000, 1, 1, 1 ),
       ( STR_TO_DATE('2021-07-06 12:30:00', '%Y-%m-%d %H:%M:%S'), 10000, 2, 2, 1 );
*/
SELECT * FROM VENTAS;
# CONSULTA DE DETALLE DE VENTAS POR USUARIO 
DROP VIEW IF EXISTS DETALLE_DE_VENTAS_POR_USUARIO ;
CREATE VIEW DETALLE_DE_VENTAS_POR_USUARIO AS
	SELECT
		V.Id AS Id_de_la_venta,
		V.Fecha AS Fecha_de_la_venta,
		V.Monto AS Monto_de_la_venta,

		C.Nombre AS Nombre_del_cliente,

		U.Nombre AS Nombre_del_usuario,

		E.Nombre AS Nombre_del_equipo_usuario
	FROM 
 VENTAS AS V INNER JOIN CLIENTES AS C 
 ON V.CLIENTES_id = C.id
 INNER JOIN USUARIOS AS U
 ON V.USUARIOS_id = U.id
 INNER JOIN EQUIPOS AS E
 ON U.EQUIPOS_id = E.id;
SELECT * FROM DETALLE_DE_VENTAS_POR_USUARIO;
SELECT * FROM DETALLE_DE_VENTAS_POR_USUARIO WHERE Nombre_del_equipo_usuario="Equipo_1";
SELECT * FROM DETALLE_DE_VENTAS_POR_USUARIO WHERE Nombre_del_usuario="Usuario 1";
# VENTAS POR EQUIPO
DROP VIEW IF EXISTS VENTAS_POR_EQUIPO;
CREATE VIEW VENTAS_POR_EQUIPO AS
	SELECT
        E.id AS ID_EQUIPOS,
		E.nombre AS Nombre_del_equipo,
		SUM(V.monto) AS Ventas_totales_por_equipo
	FROM EQUIPOS AS E INNER JOIN USUARIOS AS U
	ON E.id = U.EQUIPOS_id
	INNER JOIN VENTAS AS V
	ON V.USUARIOS_id = U.id
	GROUP BY E.nombre,E.id;
SELECT * FROM VENTAS_POR_EQUIPO;

#DROP VIEW IF EXISTS VENTAS_POR_EQUIPO_EXTRA;
#CREATE VIEW VENTAS_POR_EQUIPO_EXTRA AS
#	SELECT
#	  V.id AS Id_de_la_venta,
#	  V.Fecha Fecha_de_la_venta,
#      V.Monto Monto_de_la_venta,
#      
#	  Nombre_del_cliente,
#	  Nombre_del_equipo,
#	  Nombre_del_usuario
#	FROM EQUIPOS AS E INNER JOIN USUARIOS AS U
#	ON E.id = U.EQUIPOS_id
#	INNER JOIN VENTAS AS V
#	ON V.USUARIOS_id = U.id
#	GROUP BY E.nombre,E.id;
#SELECT * FROM VENTAS_POR_EQUIPO_EXTRA;
# VENTAS POR EQUIPO -> MES 
DROP VIEW IF EXISTS VENTAS_POR_EQUIPO_MES ;
CREATE VIEW VENTAS_POR_EQUIPO_MES AS
 SELECT
	E.nombre AS Nombre_del_equipo,
	DATE_FORMAT(V.Fecha, "%m") AS Numero_de_mes_del_year,
	SUM(V.monto) AS Ventas_totales_por_equipo_mes
 FROM EQUIPOS AS E INNER JOIN USUARIOS AS U
 ON E.id = U.EQUIPOS_id
 INNER JOIN VENTAS AS V
 ON V.USUARIOS_id = U.id
 GROUP BY E.nombre,DATE_FORMAT(V.Fecha, "%m")
 ;
SELECT * FROM VENTAS_POR_EQUIPO_MES;
# VENTAS POR USUARIO
DROP VIEW IF EXISTS VENTAS_POR_USUARIO ;
CREATE VIEW VENTAS_POR_USUARIO AS
	SELECT
		U.id AS ID_USUARIOS,
		U.nombre AS Nombre_del_usuario,
		SUM(V.monto) AS Ventas_totales_por_usuario_mes
	FROM VENTAS AS V INNER JOIN USUARIOS AS U
	ON V.USUARIOS_id = U.id
	GROUP BY U.nombre,U.id;
SELECT * FROM VENTAS_POR_USUARIO;
# VENTAS POR USUARIO MES
DROP VIEW IF EXISTS VENTAS_POR_USUARIO_MES;
CREATE VIEW VENTAS_POR_USUARIO_MES AS
	SELECT
		U.nombre AS Nombre_del_usuario,
        DATE_FORMAT(V.Fecha, "%m") AS Numero_de_mes_del_year,
		SUM(V.monto) AS Ventas_totales_por_usuario
	FROM VENTAS AS V INNER JOIN USUARIOS AS U
	ON V.USUARIOS_id = U.id
	GROUP BY U.nombre, DATE_FORMAT(V.Fecha, "%m");
SELECT * FROM VENTAS_POR_USUARIO_MES;
# VENTAS POR MES
DROP VIEW IF EXISTS VENTAS_POR_MES;
CREATE VIEW VENTAS_POR_MES AS
	SELECT
		DATE_FORMAT(V.Fecha, "%m") AS Numero_de_mes_del_year,
		SUM(V.monto) AS Ventas_totales_por_mes
	FROM VENTAS AS V
    GROUP BY DATE_FORMAT(V.Fecha, "%m");
SELECT * FROM VENTAS_POR_MES;
