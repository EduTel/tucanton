import logo from './logo.svg';
import './App.css';
import React, { useState, useEffect } from 'react';
const axios = require('axios');
const HOST = "http://127.0.0.1:8081/"
function App() {
  const [equipos, setEquipos] = useState([]);
  const [usuarios, setUsuarios] = useState([]);
  const [grid, setGrid] = useState([]);

  // De forma similar a componentDidMount y componentDidUpdate
  useEffect(() => {
    axios.get(`${HOST}equipos`).then(res => {
      console.log("equipos:")
      const data = res.data;
      setEquipos(data)
      console.log(data)
    })
    axios.get(`${HOST}usuarios`).then(res => {
      console.log("usuarios:")
      const data = res.data;
      setUsuarios(data)
      console.log(data)
    })
  },[]);
  const detalle_ventas_equipo = (e) => {
    console.log(e.target.value)
    axios.get(`${HOST}/detalle/ventas/equipo/${e.target.value}`).then(res => {
        const data = res.data;
        setGrid(data)
        console.log(data)
    })
  }
  const detalle_ventas_usuario = (e) =>{
    console.log(e.target.value)
    axios.get(`${HOST}/detalle/ventas/usuario/${e.target.value}`)
    .then(res => {
        const data = res.data;
        setGrid(data)
        console.log(data)
    })
  }

  return (
    <div className="App">
      <h1>Test</h1>
      <div className="row justify-content-md-center">
        <div className="col col-lg-2">
          <select className="form-control" name="select" onChange={detalle_ventas_equipo}>
          {
            equipos.map((reptile) => {
              return (
                <option key={reptile[1]} value={reptile[1]}>{reptile[1]}</option>
              )
            })
          }
          </select>
        </div>
        <div className="col col-lg-2">
          <select className="form-control" name="select" onChange={detalle_ventas_usuario}>
            {
              usuarios.map((reptile) => {
                return (
                  <option key={reptile[1]} value={reptile[1]}>{reptile[1]}</option>
                )
              })
            }
          </select>
        </div>
      </div>
      <div>
        <table className="table">
        {
          grid.map((reptile) => {
            return (
              <tr>
                <td>{reptile[0]}</td>
                <td>{reptile[1]}</td>
                <td>{reptile[2]}</td>
                <td>{reptile[3]}</td>
                <td>{reptile[4]}</td>
                <td>{reptile[5]}</td>
              </tr>
            )
          })
        }
        </table>
      </div>
    </div>
  );
}

export default App;
