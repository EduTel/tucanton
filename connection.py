from mysql.connector import connection

class myConectionMysql:

    mydb = None
    cursor = None

    def __init__(self, p_user="", p_password="", p_host="", p_database="", p_port=""):
        print("p_user", p_user)
        print("p_password", p_password)
        print("p_host", p_host)
        print("p_database", p_database)
        print("p_port", p_port)
        self.mydb = connection.MySQLConnection(user=p_user, password=p_password, host=p_host, database=p_database, port=p_port)
        # self.mydb = connection.MySQLConnection(user=p_user, password=p_password, host=p_host, database=p_database, port=p_port, auth_plugin='mysql_native_password')
        print("mysql", self.mydb.is_connected())
        self.cursor = self.mydb.cursor()

    def get_cursor(self):
        return self.mydb.cursor(buffered=True)

    def query(self, sql, params=()):
        self.cursor.execute(sql, params)
        self.close()
    
    def close(self):
        self.mydb.close()

import psycopg2
class myConectionPostgreSQL:

    mydb = None
    cursor = None

    def __init__(self, p_user="", p_password="", p_host="", p_database="", p_port=""):
        print("p_user", p_user)
        print("p_password", p_password)
        print("p_host", p_host)
        print("p_database", p_database)
        print("p_port", p_port)
        self.mydb = psycopg2.connect(user=p_user, password=p_password, host=p_host, database=p_database, port=p_port)
        # self.mydb = connection.MySQLConnection(user=p_user, password=p_password, host=p_host, database=p_database, port=p_port, auth_plugin='mysql_native_password')
        #print("mysql", self.mydb.is_connected())
        self.cursor = self.mydb.cursor()

    def get_cursor(self):
        return self.mydb.cursor(buffered=True)

    def query(self, sql, params=()):
        self.cursor.execute(sql, params)
    
    def close(self):
        self.mydb.close()