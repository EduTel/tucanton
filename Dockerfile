FROM python:3.9.1-alpine3.13
LABEL version="1.0"
LABEL description="TuCanton Flask"
LABEL maintainer = ["eduardo_jonathan@outlook.com"]
WORKDIR /code
ENV FLASK_APP=index.py
ENV FLASK_RUN_HOST=0.0.0.0
RUN apk add --no-cache gcc musl-dev linux-headers
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
EXPOSE 5000/tcp
COPY ./connection.py .
COPY ./index.py .
#CMD ["flask", "run"]
# Specify the command to run on container start
CMD ["python", "./index.py"]