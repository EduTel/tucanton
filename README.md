# INSTRUCCIONES
en el directorio root del proyecto ejecutar docker-compose build y despues docker-compose up -d despues dentro de la carpeta frontend npm install y npm start para ejecutar la parte de react, tiene que esperar a que termine de cargar el docker-compose para poder recargar la pagina de react y poder usar el app

1. La manera mas fácil de ejecutar es descargar los contenedores desde Docker hub  
    - docker push edutel/react_tucanton:tagname
    - docker push edutel/mysql_tucanton:tagname
    - docker push edutel/flask_tucanton:tagname

# TEST
## DESARROLLO WEB
Menciona 3 frameworks de desarrollo del lado del cliente?  
- React, Angular y Vue  

Menciona 3 frameworks de desarrollo del lado del servidor? 
- Django, Flask y FastAPI  

Menciona 3 servidores web?  
- APACHE, IIS y NGINX

Menciona 3 gestores de base de datos sql?  
- MySQL, PostgreSQL, SQLite

Menciona 2 gestores de base de datos nosql?  
- MongoDB y Cassandra

Explica brevemente el modelo MVC?  
- Modelo-vista-controlador (MVC) es un patrón de arquitectura de software, que separa los datos y principalmente lo que es la lógica de negocio de una aplicación de su representación y el módulo encargado de gestionar los eventos y las comunicaciones. 

Realiza un diagrama que explique como funciona una arquitectura web
![](https://edteam-media.s3.amazonaws.com/blogs/original/a04ca961-f61d-4dc3-837c-55c06df42ce7.png)

## LINUX
¿Cual de estas no es una distribucion Linux?

- Debian
- ## **Citrix**
- Red Hat
- Caldera
- Mandriva
- Ubuntu  

¿Cual no es un entorno de escritorio en Linux?

- GNOME
- ## **GNODE**
- XFCE
- KDE

¿Que comando utilizariamos para eliminar todo el contenido de la carpeta /borrarCarpeta ?

- rm /borrarCarpeta/*.*
- ## **rm -r /borrarCarpeta**
- rm -r /borrarCarpeta/*
- rm /borrarCarpeta

¿Como se podria mostrar el contenido de un archivo llamado foo?

- cat -m foo
- cat -nn foo
- ## **cat -n foo**
- cat +n foo

¿Cual de los siguientes NO ES un directorio de la estructura de LINUX?

- /etc
- /
- /dev
- /mmt.

¿El Directorio /etc es que el almacena los archivos de configuracion del Sistema Operativo?

- ## **Verdadero**
- Falso.

¿En el directorio /home se almacena....?

- Directorios de impresoras
- Directorios del Sistema
- ## **Directorios de Usuarios**
- Directorios temporales

¿Comando que nos permite observar las 10 ultimas lineas de un archivo?

- tail ejecutar.sh
- tail -10 ejecutar.sh
- ## **tail -n 10 ejecutar.sh**

¿En el directorio /usr/man encontramos los manuales del sistema operativo?
- ## **Falso**
- Verdadero

¿El comando que permite cambiar de usuario es?
- su

¿El comando que me permite colocar permisos a un archivo es?  
- chmod

¿El comando que me permite crear un usuario es?  
useradd

¿El comando que me permite "montar" un dispositivo en Linux es?  
- ## **mount**
- umount
- moount
- mouunt

¿Comando que me permite crear directorios?
- mkdir

¿comando que me permite borrar un directorio y su contenido, sin preguntar nada?
- rm -rf