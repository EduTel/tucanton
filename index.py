# -*- coding: utf-8 -*-
from connection import myConectionMysql, myConectionPostgreSQL
from flask import request
from pprint import pprint
import os
import json
import datetime
import time
from flask import Flask
# from flask_mysqldb import MySQL

from flask_cors import CORS
app = Flask(__name__)
# app.config['MYSQL_HOST'] = '127.0.0.1'
# app.config['MYSQL_USER'] = 'root'
# app.config['MYSQL_PASSWORD'] = ''
# app.config['MYSQL_DB'] = 'TuCanton'
# mysql = MySQL(app)
# app.config['CORS_HEADERS'] = 'Content-Type'
cors = CORS(app, resources={r"*": {"origins": "*"}})


from dotenv import load_dotenv
load_dotenv()


def coneccion():
    sqlI = myConectionMysql("root", "root", "mysqldb", "TuCanton", 3306) # coneccion para bridge docker
    #sqlI = myConectionPostgreSQL("smyrgxhkbgrlcm", "41e390e2917a54b7353e704bb33c94c687beea8ee74c14c45296089eb166c72d", "ec2-54-225-228-142.compute-1.amazonaws.com", "dbmnbioa5hsr9n", 5432) # coneccion para bridge docker
    #sqlI = myConectionMysql("edutel2", "edutel_tucanton", "db4free.net", "edutel_tucanton", 3306) # coneccion para bridge docker
    #sqlI = myConectionMysql("KHljaCPgef", "bqJwSiEAvW", "remotemysql.com", "KHljaCPgef", 3306) # coneccion para bridge docker
    #sqlI = myConectionMysql(os.getenv("MYSQL_USER"), os.getenv("MYSQL_PASSWORD"), os.getenv("MYSQL_HOST"), "TuCanton", 3306) # coneccion para bridge docker
    #sqlI = myConectionMysql("root", "root", "127.0.0.1", "TuCanton", 3306) # coneccion para mysql local
    #sqlI = myConectionMysql("root", "root", os.getenv("MYSQL_HOST"), "TuCanton", os.getenv("MYSQL_PORT")) # coneccion para mysql local
    return sqlI

@app.route('/clientes', methods=['GET'])
def get_clientes():
    #cur = mysql.connection.cursor()
    sqlI = coneccion()
    sqlI.query("SELECT * FROM CLIENTES")
    data = sqlI.cursor.fetchall()
    # pprint(data)
    response = app.response_class(
        response=json.dumps(data),
        mimetype='application/json'
    )
    return response

@app.route('/clientes/<id>', methods=['GET'])
def get_clientes_id(id):
    print("get_clientes_id")
    print("id", id)
    #cur = mysql.connection.cursor()
    sqlI = coneccion()
    sqlI.query("SELECT * FROM CLIENTES WHERE id=%s", (id,))
    data = sqlI.cursor.fetchall()
    #pprint(data)
    response = app.response_class(
        response=json.dumps(data),
        mimetype='application/json'
    )
    return response


@app.route('/usuarios', methods=['GET'])
def get_usuarios():
    print("get_usuarios")
    #cur = mysql.connection.cursor()
    sqlI = coneccion()
    sqlI.query("SELECT * FROM USUARIOS")
    #return "[]"
    data = sqlI.cursor.fetchall()
    #pprint(data)
    response = app.response_class(
        response=json.dumps(data),
        mimetype='application/json'
    )
    print("response")
    return response


@app.route('/usuarios/<id>', methods=['GET'])
def get_usuarios_id(id):
    print("get_usuarios_id")
    print("id", id)
    #cur = mysql.connection.cursor()
    sqlI = coneccion()
    sqlI.query("SELECT * FROM USUARIOS WHERE id=%s", (id,))
    data = sqlI.cursor.fetchall()
    #pprint(data)
    response = app.response_class(
        response=json.dumps(data),
        mimetype='application/json'
    )
    return response


@app.route('/equipos', methods=['GET'])
def get_equipos():
    print("get_equipos")
    #return "[]"
    sqlI = coneccion()
    #print("mysql", sqlI.mydb.is_connected())
    #cur = mysql.connection.cursor()
    sqlI.query("SELECT * FROM EQUIPOS")
    data = sqlI.cursor.fetchall()
    pprint(data)
    response = app.response_class(
        response=json.dumps(data),
        mimetype='application/json'
    )
    print("response")
    return response

@app.route('/equipos/<id>', methods=['GET'])
def get_equipos_id(id):
    print("get_equipos_id")
    print("id", id)
    #cur = mysql.connection.cursor()
    sqlI = coneccion()
    sqlI.query("SELECT * FROM EQUIPOS WHERE id=%s", (id,))
    data = sqlI.cursor.fetchall()
    #pprint(data)
    response = app.response_class(
        response=json.dumps(data),
        mimetype='application/json'
    )
    return response

@app.route('/ventas/equipo/<id>', methods=['GET'])
def get_ventas_equipo_id(id):
    print("get_ventas_equipo_id")
    print("id", id)
    #cur = mysql.connection.cursor()
    sqlI = coneccion()
    sqlI.query("SELECT * FROM VENTAS_POR_EQUIPO WHERE ID_EQUIPOS=%s", (id,))
    data = sqlI.cursor.fetchall()
    #pprint(data)
    response = app.response_class(
        response=json.dumps(data),
        mimetype='application/json'
    )
    return response

@app.route('/ventas/usuario/<id>', methods=['GET'])
def get_ventas_usuario_id(id):
    print("get_ventas_usuario_id")
    print("id", id)
    #cur = mysql.connection.cursor()
    sqlI = coneccion()
    sqlI.query("SELECT * FROM VENTAS_POR_USUARIO WHERE ID_USUARIOS=%s", (id,))
    data = sqlI.cursor.fetchall()
    #pprint(data)
    response = app.response_class(
        response=json.dumps(data),
        mimetype='application/json'
    )
    return response

def myconverter(o):
    if isinstance(o, datetime.datetime):
        return o.__str__()
    if isinstance(o, datetime.date):
        return o.__str__()

@app.route('/detalle/ventas/equipo/<nombre>', methods=['GET'])
def get_detalle_ventas_equipo_nombre(nombre):
    print("get_detalle_ventas_equipo_nombre")
    print("nombre:", nombre)
    #cur = mysql.connection.cursor()
    sqlI = coneccion()
    sqlI.query("SELECT * FROM DETALLE_DE_VENTAS_POR_USUARIO WHERE Nombre_del_equipo_usuario=%s", (nombre,))
    data = sqlI.cursor.fetchall()
    print("data")
    pprint(data)
    print(type(data[0][1]))
    response = app.response_class(
        response=json.dumps(data, default = myconverter),
        mimetype='application/json'
    )
    return response

@app.route('/detalle/ventas/usuario/<nombre>', methods=['GET'])
def get_detalle_ventas_usuario_nombre(nombre):
    print("nombre:", nombre)
    #cur = mysql.connection.cursor()
    sqlI = coneccion()
    sqlI.query("SELECT * FROM DETALLE_DE_VENTAS_POR_USUARIO WHERE Nombre_del_usuario=%s", (nombre,))
    data = sqlI.cursor.fetchall()
    #pprint(data)
    response = app.response_class(
        response=json.dumps(data, default = myconverter),
        mimetype='application/json'
    )
    return response

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0", port=os.getenv("FLASK_PORT"), threaded=True)